package de.codecamp.vaadin.eventbus.impl;


import static java.util.Objects.requireNonNull;

import de.codecamp.vaadin.eventbus.EventBusSubscription;
import org.springframework.core.ResolvableType;


/**
 * {@link ListenerAdapter} instances provide the actual logic to deliver events to a specific
 * listener.
 */
public abstract class ListenerAdapter
{

  private final ResolvableType expectedEventType;

  private EventBusSubscription eventBusSubscription;


  /**
   * Constructs a new instance.
   *
   * @param expectedEventType
   *          the expected event type
   */
  protected ListenerAdapter(ResolvableType expectedEventType)
  {
    this.expectedEventType =
        requireNonNull(expectedEventType, "expectedEventType must not be null");
  }


  /**
   * Returns the event bus subscription.
   *
   * @return the event bus subscription
   */
  protected EventBusSubscription getSubscription()
  {
    return eventBusSubscription;
  }

  /**
   * Initializes the {@link EventBusSubscription}.
   *
   * @param subscription
   *          the event bus subscription
   */
  protected void initSubscription(EventBusSubscription subscription)
  {
    if (this.eventBusSubscription != null)
      throw new IllegalStateException("The event bus registration has already be initialized.");

    this.eventBusSubscription = requireNonNull(subscription, "subscription must not be null");
  }

  /**
   * Returns the expected event type.
   *
   * @return the expected event type
   */
  protected ResolvableType getExpectedEventType()
  {
    return expectedEventType;
  }

  /**
   * Handle an event.
   *
   * @param event
   *          the event to handle
   */
  protected abstract void processEventBusEvent(Object event)
    throws Throwable;

}
