package de.codecamp.vaadin.eventbus.impl;


import static de.codecamp.vaadin.eventbus.impl.EventBusImpl.EVENT_TYPE_CACHE;

import de.codecamp.vaadin.eventbus.EventBusListener;
import org.springframework.core.ResolvableType;


/**
 * A {@link ListenerAdapter} that wraps an {@link EventBusListener} and uses an explicitly provided
 * expected event type.
 */
public class ExplicitTypeListenerAdapter
  extends
    ListenerAdapter
{

  @SuppressWarnings("rawtypes")
  private final EventBusListener listener;


  /**
   * Constructs a new instance.
   *
   * @param <EVENT>
   *          the expected event type
   * @param listener
   *          the actual event listener
   * @param expectedEventType
   *          the expected event type
   */
  public <EVENT> ExplicitTypeListenerAdapter(EventBusListener<EVENT> listener,
      Class<EVENT> expectedEventType)
  {
    super(EVENT_TYPE_CACHE.computeIfAbsent(expectedEventType,
        c -> ResolvableType.forClass(expectedEventType)));
    this.listener = listener;
  }


  @Override
  @SuppressWarnings({"unchecked"})
  protected void processEventBusEvent(Object event)
  {
    listener.onBusEvent(event);
  }


  @Override
  public String toString()
  {
    return "'" + listener.toString() + "'";
  }

}
