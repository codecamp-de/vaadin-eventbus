package de.codecamp.vaadin.eventbus.impl;


import static java.util.Objects.requireNonNull;

import de.codecamp.vaadin.eventbus.EventBus;
import de.codecamp.vaadin.eventbus.EventBusAccessor;


public class EventBusAccessorImpl
  implements
    EventBusAccessor
{

  private final EventBus applicationScopeEventBus;

  private final EventBus sessionScopeEventBus;

  private final EventBus uiScopeEventBus;


  public EventBusAccessorImpl(EventBus applicationScopeEventBus, EventBus sessionScopeEventBus,
      EventBus uiScopeEventBus)
  {
    this.applicationScopeEventBus = applicationScopeEventBus;
    this.sessionScopeEventBus =
        requireNonNull(sessionScopeEventBus, "sessionScopeEventBus must not be null");
    this.uiScopeEventBus = requireNonNull(uiScopeEventBus, "uiScopeEventBus must not be null");
  }


  @Override
  public EventBus applicationScope()
  {
    if (applicationScopeEventBus == null)
      throw new IllegalStateException("The application scope event bus has not been enabled.");
    return applicationScopeEventBus;
  }

  @Override
  public EventBus sessionScope()
  {
    return sessionScopeEventBus;
  }

  @Override
  public EventBus uiScope()
  {
    return uiScopeEventBus;
  }

}
