package de.codecamp.vaadin.eventbus.impl;


import static de.codecamp.vaadin.eventbus.impl.EventBusImpl.EVENT_TYPE_CACHE;

import de.codecamp.vaadin.eventbus.EventBusListener;
import org.springframework.core.ResolvableType;


/**
 * A {@link ListenerAdapter} that wraps an {@link EventBusListener} and that determines the expected
 * event type from that provided {@link EventBusListener}.
 */
public class ImplicitTypeListenerAdapter
  extends
    ListenerAdapter
{

  @SuppressWarnings("rawtypes")
  private final EventBusListener listener;


  /**
   * Constructs a new instance.
   *
   * @param listener
   *          the actual event listener
   */
  public ImplicitTypeListenerAdapter(EventBusListener<?> listener)
  {
    super(EVENT_TYPE_CACHE.computeIfAbsent(listener.getClass(),
        c -> ResolvableType.forClass(listener.getClass()).as(EventBusListener.class).getGeneric()));
    this.listener = listener;
  }


  @Override
  @SuppressWarnings({"unchecked"})
  protected void processEventBusEvent(Object event)
  {
    listener.onBusEvent(event);
  }


  @Override
  public String toString()
  {
    return "'" + listener.toString() + "'";
  }

}
