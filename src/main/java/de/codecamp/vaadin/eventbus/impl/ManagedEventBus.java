package de.codecamp.vaadin.eventbus.impl;


import com.vaadin.flow.shared.Registration;
import de.codecamp.vaadin.eventbus.EventBus;
import java.util.Map;


/**
 * An {@link EventBus} with additional methods used for internal management and communication
 * between event buses that are not part of the public API.
 */
public interface ManagedEventBus
  extends
    EventBus
{

  /**
   * Registers an {@link EventBus} for a child scope.
   *
   * @param eventBus
   *          the child event bus
   * @return the registration
   */
  ChildEventBusRegistration registerChildEventBus(ManagedEventBus eventBus);


  void publishStickyFromParent(Object event);

  Map<Class<?>, Object> getStickyEventsForDispatch(Map<Class<?>, Object> stickyEvents);


  /**
   * Registration for a child event bus.
   */
  interface ChildEventBusRegistration
    extends
      Registration
  {

    ManagedEventBus getParentEventBus();

    EventBus getChildEventBus();

  }

}
