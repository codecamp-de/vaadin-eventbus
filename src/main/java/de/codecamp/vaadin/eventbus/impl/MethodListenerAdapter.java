package de.codecamp.vaadin.eventbus.impl;


import static de.codecamp.vaadin.eventbus.impl.EventBusImpl.EVENT_TYPE_CACHE;
import static java.util.Objects.requireNonNull;

import java.lang.invoke.MethodHandle;
import java.lang.invoke.MethodHandles;
import java.lang.invoke.MethodType;
import java.lang.reflect.Method;
import org.springframework.core.ResolvableType;


/**
 * A {@link ListenerAdapter} that uses reflection to deliver the event.
 */
public class MethodListenerAdapter
  extends
    ListenerAdapter
{

  private final Object object;

  private final String methodName;

  private transient MethodHandle methodHandle;


  /**
   * Constructs a new instance.
   *
   * @param object
   *          the object on which to call the method
   * @param listenerMethod
   *          the listener method to be called on the provided object
   */
  public MethodListenerAdapter(Object object, Method listenerMethod)
  {
    super(EVENT_TYPE_CACHE.computeIfAbsent(
        requireNonNull(listenerMethod, "listenerMethod must not be null"),
        c -> ResolvableType.forMethodParameter(listenerMethod, 0)));

    this.object = requireNonNull(object, "object must not be null");

    this.methodName = listenerMethod.getName();
  }


  @Override
  protected void processEventBusEvent(Object event)
    throws Throwable
  {
    if (methodHandle == null)
    {
      try
      {
        MethodType methodType =
            MethodType.methodType(void.class, getExpectedEventType().getRawClass());
        methodHandle =
            MethodHandles.publicLookup().findVirtual(object.getClass(), methodName, methodType);
      }
      catch (Exception ex)
      {
        throw new RuntimeException( // NOPMD:AvoidThrowingRawExceptionTypes not creating an exception just for this
            String.format("Failed to acquire MethodHandle for listener method {}#{}({}).",
                object.getClass().getName(), methodName, getExpectedEventType().toClass()),
            ex);
      }
    }

    methodHandle.invoke(object, event);
  }


  @Override
  public String toString()
  {
    return "method " + object.getClass().getName() + "#" + methodName + "("
        + getExpectedEventType().toString() + ") on object '" + object.toString() + "'";
  }

}
