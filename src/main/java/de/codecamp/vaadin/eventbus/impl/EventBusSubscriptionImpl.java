package de.codecamp.vaadin.eventbus.impl;


import static java.util.Objects.requireNonNull;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.function.SerializableConsumer;
import com.vaadin.flow.shared.Registration;
import de.codecamp.vaadin.eventbus.EventBusSubscription;


public class EventBusSubscriptionImpl
  implements
    EventBusSubscription
{

  private final ListenerAdapter listener;

  private final SerializableConsumer<ListenerAdapter> activateListener;

  private final SerializableConsumer<ListenerAdapter> deactivateListener;

  private Registration attachRegistration;

  private Registration detachRegistration;


  public EventBusSubscriptionImpl(ListenerAdapter listener,
      SerializableConsumer<ListenerAdapter> activateListener,
      SerializableConsumer<ListenerAdapter> deactivateListener)
  {
    this.listener = requireNonNull(listener, "listener must not be null");
    this.activateListener = requireNonNull(activateListener, "activateListener must not be null");
    this.deactivateListener =
        requireNonNull(deactivateListener, "deactivateListener must not be null");

    activateListener.accept(listener);
  }


  @Override
  public void remove()
  {
    deactivateListener.accept(listener);
    unbind();
  }

  /**
   * Bind this subscription to the lifecycle of the given component. I.e. the listener is registered
   * exactly as long as the component is attached. When bound it's usually no longer necessary to
   * manually {@link #remove() unregister} from the event bus.
   * <p>
   * Use null to unbind. I.e. the subscription is active until manually {@link #remove()
   * unregistered}.
   *
   * @param component
   *          the component to bind the subscription to; null to unbind
   */
  @Override
  public void bindTo(Component component)
  {
    unbind();

    if (component == null)
    {
      deactivateListener.accept(listener);
    }
    else
    {
      attachRegistration = component.addAttachListener(e -> activateListener.accept(listener));
      detachRegistration = component.addDetachListener(e -> deactivateListener.accept(listener));

      if (component.isAttached())
        activateListener.accept(listener);
      else
        deactivateListener.accept(listener);
    }
  }

  @Override
  public void unbind()
  {
    if (attachRegistration != null)
    {
      attachRegistration.remove();
      attachRegistration = null;
    }
    if (detachRegistration != null)
    {
      detachRegistration.remove();
      detachRegistration = null;
    }
  }

}
