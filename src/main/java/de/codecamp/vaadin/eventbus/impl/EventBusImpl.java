package de.codecamp.vaadin.eventbus.impl;


import static java.util.Objects.requireNonNull;

import de.codecamp.vaadin.eventbus.EventBus;
import de.codecamp.vaadin.eventbus.EventBusListener;
import de.codecamp.vaadin.eventbus.EventBusSubscription;
import de.codecamp.vaadin.eventbus.EventScope;
import java.lang.reflect.Method;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.CopyOnWriteArrayList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.ResolvableType;
import org.springframework.util.ConcurrentReferenceHashMap;


public class EventBusImpl
  implements
    ManagedEventBus
{

  /**
   * A shared cache for {@link ResolvableType} instances for actual or expected event types.
   */
  public static final ConcurrentMap<Object, ResolvableType> EVENT_TYPE_CACHE =
      new ConcurrentReferenceHashMap<>();


  private static final Logger LOG = LoggerFactory.getLogger(EventBusImpl.class);


  private final String name;

  private final EventScope eventScope;


  private final ChildEventBusRegistration parentRegistration;

  private final Collection<ManagedEventBus> childEventBuses =
      Collections.newSetFromMap(new ConcurrentHashMap<>());


  private final Collection<ListenerAdapter> listeners;

  private final Map<Class<?>, Object> stickyEvents = new ConcurrentHashMap<>();


  public EventBusImpl(String name, EventScope eventScope)
  {
    this(name, eventScope, null);
  }

  public EventBusImpl(String name, EventScope eventScope, ManagedEventBus parent)
  {
    this.name = name;
    this.eventScope = requireNonNull(eventScope, "eventScope must not be null");

    if (eventScope == EventScope.APPLICATION)
      this.listeners = Collections.newSetFromMap(new ConcurrentHashMap<>());
    else
      this.listeners = new CopyOnWriteArrayList<>();

    if (parent != null)
    {
      this.parentRegistration = parent.registerChildEventBus(this);
    }
    else
    {
      this.parentRegistration = null;
    }
  }


  public void destroy()
  {
    if (parentRegistration != null)
      parentRegistration.remove();
  }


  /**
   * Returns the name of this event bus.
   *
   * @return the name of this event bus
   */
  public String getName()
  {
    return name;
  }

  @Override
  public EventScope getScope()
  {
    return eventScope;
  }


  @Override
  public ChildEventBusRegistration registerChildEventBus(ManagedEventBus childEventBus)
  {
    if (!childEventBuses.add(childEventBus))
      throw new IllegalArgumentException("Event bus already registered.");

    return new ChildEventBusRegistrationImpl(childEventBus);
  }

  @Override
  public Map<Class<?>, Object> getStickyEventsForDispatch(Map<Class<?>, Object> stickyEvents)
  {
    if (stickyEvents == null)
      stickyEvents = new HashMap<>();

    if (parentRegistration != null)
      parentRegistration.getParentEventBus().getStickyEventsForDispatch(stickyEvents);

    stickyEvents.putAll(this.stickyEvents);

    return stickyEvents;
  }

  @Override
  public <EVENT> EventBusSubscription subscribe(EventBusListener<EVENT> listener)
  {
    return subscribe(new ImplicitTypeListenerAdapter(listener));
  }

  @Override
  public <EVENT> EventBusSubscription subscribe(Class<EVENT> expectedEventType,
      EventBusListener<EVENT> listener)
  {
    return subscribe(new ExplicitTypeListenerAdapter(listener, expectedEventType));
  }

  @Override
  public EventBusSubscription subscribe(Object bean, Method listenerMethod)
  {
    return subscribe(new MethodListenerAdapter(bean, listenerMethod));
  }

  private EventBusSubscription subscribe(ListenerAdapter listener)
  {
    EventBusSubscriptionImpl subscription =
        new EventBusSubscriptionImpl(listener, this::activateListener, this::deactivateListener);
    listener.initSubscription(subscription);
    return subscription;
  }

  private void activateListener(ListenerAdapter listener)
  {
    if (!listeners.contains(listener))
    {
      listeners.add(listener);

      getStickyEventsForDispatch(null).values()
          .forEach(event -> dispatchEventToListener(event, listener));
    }
  }

  private void deactivateListener(ListenerAdapter listener)
  {
    listeners.remove(listener);
  }

  @Override
  public void publish(Object event)
  {
    for (ListenerAdapter listener : listeners)
    {
      dispatchEventToListener(event, listener);
    }

    for (EventBus childEventBus : childEventBuses)
    {
      childEventBus.publish(event);
    }
  }

  @Override
  public void publishSticky(Object event)
  {
    stickyEvents.put(event.getClass(), event);
    dispatchStickyEvent(event);
  }

  @Override
  public void publishStickyFromParent(Object event)
  {
    /* If child event bus has sticky event of the same type, don't dispatch it there. */
    if (stickyEvents.containsKey(event.getClass()))
      return;

    dispatchStickyEvent(event);
  }

  private void dispatchStickyEvent(Object event)
  {
    for (ListenerAdapter listener : listeners)
    {
      dispatchEventToListener(event, listener);
    }

    for (ManagedEventBus childEventBus : childEventBuses)
    {
      childEventBus.publishStickyFromParent(event);
    }
  }

  @Override
  public <T> T getSticky(Class<T> eventType)
  {
    return eventType.cast(stickyEvents.get(eventType));
  }

  @Override
  public <T> T removeSticky(Class<T> eventType)
  {
    return eventType.cast(stickyEvents.remove(eventType));
  }

  private void dispatchEventToListener(Object event, ListenerAdapter listener)
  {
    try
    {
      ResolvableType actualEventType = EVENT_TYPE_CACHE.computeIfAbsent(event.getClass(),
          c -> ResolvableType.forClass(event.getClass()));

      if (!listener.getExpectedEventType().isAssignableFrom(actualEventType))
        return;

      listener.processEventBusEvent(event);
    }
    catch (Throwable ex) // NOPMD:AvoidCatchingThrowable
    {
      LOG.error("Failed to deliver event '{}' of type {} to listener {} in {} scope.", event,
          event.getClass().getName(), listener, eventScope, ex);
    }
  }


  /* package */ class ChildEventBusRegistrationImpl
    implements
      ChildEventBusRegistration
  {

    private final EventBus childEventBus;


    ChildEventBusRegistrationImpl(EventBus childEventBus)
    {
      this.childEventBus = childEventBus;
    }


    @Override
    public ManagedEventBus getParentEventBus()
    {
      return EventBusImpl.this;
    }

    @Override
    public EventBus getChildEventBus()
    {
      return childEventBus;
    }


    @Override
    public void remove()
    {
      childEventBuses.remove(childEventBus);
    }

  }

}
