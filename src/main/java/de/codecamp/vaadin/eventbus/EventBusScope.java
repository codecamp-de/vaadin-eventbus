package de.codecamp.vaadin.eventbus;


import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import org.springframework.beans.factory.annotation.Qualifier;


/**
 * A {@link Qualifier} annotation to select a specific {@link EventBus}.
 */
@Target({ElementType.FIELD, ElementType.METHOD, ElementType.PARAMETER, ElementType.TYPE,
    ElementType.ANNOTATION_TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Qualifier
@Documented
public @interface EventBusScope
{

  /**
   * @return the scope of the event (bus)
   */
  EventScope value();

  /**
   * @return whether to the select actual event bus or a <a href=
   *         "https://docs.spring.io/spring-framework/docs/current/reference/html/core.html#beans-factory-scopes-other-injection">scoped
   *         proxy</a>
   */
  boolean proxy() default false;

}
