package de.codecamp.vaadin.eventbus;


import com.vaadin.flow.component.Component;
import java.io.Serializable;
import java.lang.reflect.Method;


/**
 * An event bus associated with a specific {@link EventScope scope}. Usually accessed through the
 * {@link EventBusAccessor}.
 */
public interface EventBus
  extends
    Serializable
{

  /**
   * Returns the scope this this event bus.
   *
   * @return the scope this this event bus
   */
  EventScope getScope();


  /**
   * Subscribes the given listener to events from the scope of this event bus. If the listener is a
   * lambda expression, consider using {@link #subscribe(Class, EventBusListener)} to specify the
   * expected event type.
   * <p>
   * Please take care to unsubscribe where and when appropriate to avoid leaks and other side
   * effects. In order to facilitate this it is possible to
   * {@link EventBusSubscription#bindTo(Component) bind the subscription} to the lifecycle of a
   * {@link Component}; i.e. when bound the listener will be registered (and receive events) only as
   * long as the component is attached.
   *
   * @param <EVENT>
   *          the expected event type
   * @param listener
   *          the listener
   * @return the event bus subscription
   */
  <EVENT> EventBusSubscription subscribe(EventBusListener<EVENT> listener);

  /**
   * Subscribes the given listener to events from the scope of this event bus.
   * <p>
   * Please take care to unsubscribe where and when appropriate to avoid leaks and other side
   * effects. In order to facilitate this it is possible to
   * {@link EventBusSubscription#bindTo(Component) bind the subscription} to the lifecycle of a
   * {@link Component}; i.e. when bound the listener will be registered (and receive events) only as
   * long as the component is attached.
   *
   * @param <EVENT>
   *          the expected event type
   * @param expectedEventType
   *          the expected event type
   * @param listener
   *          the listener
   * @return the event bus subscription
   */
  <EVENT> EventBusSubscription subscribe(Class<EVENT> expectedEventType,
      EventBusListener<EVENT> listener);

  /**
   * Subscribes the given listener to events from the scope of this event bus.
   * <p>
   * Please take care to unsubscribe where and when appropriate to avoid leaks and other side
   * effects. In order to facilitate this it is possible to
   * {@link EventBusSubscription#bindTo(Component) bind the subscription} to the lifecycle of a
   * {@link Component}; i.e. when bound the listener will be registered (and receive events) only as
   * long as the component is attached.
   *
   * @param bean
   *          the object on which to call the method
   * @param listenerMethod
   *          the listener method to call on the object
   * @return the event bus subscription
   */
  EventBusSubscription subscribe(Object bean, Method listenerMethod);


  /**
   * Publishes the given event.
   *
   * @param event
   *          the event object
   */
  void publish(Object event);


  /**
   * Publishes the given event as sticky. There can only be a single sticky event event per type.
   *
   * @param event
   *          the event object
   */
  void publishSticky(Object event);

  /**
   * Returns the sticky event of the given type.
   *
   * @param <T>
   *          the event type
   * @param eventType
   *          the event type
   * @return the event or null if no event was found
   */
  <T> T getSticky(Class<T> eventType);

  /**
   * Removes and returns the sticky event of the given type.
   *
   * @param <T>
   *          the event type
   * @param eventType
   *          the event type
   * @return the removed event or null if no event was found
   */
  <T> T removeSticky(Class<T> eventType);

}
