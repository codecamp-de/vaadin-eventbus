package de.codecamp.vaadin.eventbus;


import java.io.Serializable;
import java.util.EventListener;


/**
 * A listener interface to receive events from an {@link EventBus}.
 *
 * @param <EVENT>
 *          the type of events to be received
 */
@FunctionalInterface
public interface EventBusListener<EVENT>
  extends
    EventListener,
    Serializable

{

  /**
   * Handle an event.
   *
   * @param event
   *          the event to handle
   */
  void onBusEvent(EVENT event);

}
