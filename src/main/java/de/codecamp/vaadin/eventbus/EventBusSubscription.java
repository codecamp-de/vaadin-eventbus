package de.codecamp.vaadin.eventbus;


import com.vaadin.flow.component.Component;
import com.vaadin.flow.shared.Registration;


/**
 * A subscription to an {@link EventBus}.
 * <p>
 * Care needs to be taken to {@link #remove() unregister} where and when necessary. To facilitate
 * this it is possible to {@link #bindTo(Component) bind a subscription} to the lifecycle of a
 * {@link Component}. In that case the subscription will be active as long as the associated
 * component is {@link Component#isAttached() attached)}.
 */
public interface EventBusSubscription
  extends
    Registration
{

  /**
   * Binds this subscription to the lifecycle of the given component. I.e. the listener is
   * registered exactly as long as the component is attached. When bound it's usually no longer
   * necessary to manually {@link #remove() unregister} from the event bus.
   *
   * @param component
   *          the component to bind the subscription to; null to unbind
   */
  void bindTo(Component component);

  /**
   * Unbinds this subscription from any component. I.e. the subscription is active until manually
   * {@link #remove() unregistered}.
   */
  void unbind();

}
