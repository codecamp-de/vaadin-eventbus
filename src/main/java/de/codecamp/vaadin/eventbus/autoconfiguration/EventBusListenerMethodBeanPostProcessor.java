package de.codecamp.vaadin.eventbus.autoconfiguration;


import com.vaadin.flow.component.Component;
import com.vaadin.flow.spring.scopes.VaadinUIScope;
import de.codecamp.vaadin.eventbus.EventBus;
import de.codecamp.vaadin.eventbus.EventBusAccessor;
import de.codecamp.vaadin.eventbus.EventBusListener;
import de.codecamp.vaadin.eventbus.EventBusListenerMethod;
import de.codecamp.vaadin.eventbus.EventBusSubscription;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.stream.Stream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.support.MergedBeanDefinitionPostProcessor;
import org.springframework.beans.factory.support.RootBeanDefinition;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.util.ReflectionUtils;


public class EventBusListenerMethodBeanPostProcessor
  implements
    MergedBeanDefinitionPostProcessor
{

  private static final Logger LOG =
      LoggerFactory.getLogger(EventBusListenerMethodBeanPostProcessor.class);


  private final ConcurrentMap<String, String> beanScopeCache = new ConcurrentHashMap<>();

  private final ConcurrentMap<String, Method[]> beanMethodCache = new ConcurrentHashMap<>();


  private final EventBusAccessor eventBus;


  public EventBusListenerMethodBeanPostProcessor(EventBusAccessor eventBus)
  {
    this.eventBus = eventBus;
  }


  @Override
  public void postProcessMergedBeanDefinition(RootBeanDefinition beanDefinition, Class<?> beanType,
      String beanName)
  {
    /* determine if the bean has any subscribed methods and cache the scope */
    beanScopeCache.computeIfAbsent(beanName, bn ->
    {
      String beanScope = beanDefinition.getScope();

      boolean hasSubscribers = false;


      boolean validBean = Component.class.isAssignableFrom(beanType)
          || beanScope.equals(VaadinUIScope.VAADIN_UI_SCOPE_NAME);

      if (EventBusListener.class.isAssignableFrom(beanType))
      {
        if (validBean)
        {
          hasSubscribers = true;
        }
        else
        {
          LOG.error(
              "Could not subscripe {}-implementing bean {} [{}]. Implicit subscription only possible"
                  + " on Components or beans in Vaadin's @UIScope.",
              EventBusListener.class.getSimpleName(), beanName, beanType.getName());
        }
      }

      Method[] methods = Stream
          .of(ReflectionUtils.getUniqueDeclaredMethods(beanType,
              ReflectionUtils.USER_DECLARED_METHODS)) //
          .filter(method -> AnnotationUtils.findAnnotation(method,
              EventBusListenerMethod.class) != null) //
          .filter(method ->
          {
            boolean validMethod = !Modifier.isStatic(method.getModifiers()) //
                && Modifier.isPublic(method.getModifiers()) //
                && method.getReturnType() == void.class //
                && method.getParameterCount() == 1;

            if (!validBean)
            {
              LOG.error(
                  "Could not subscripe @{}-annotated method '{}' on bean {} [{}]. Annotation can"
                      + " only be used on Components or beans in Vaadin's @UIScope.",
                  EventBusListenerMethod.class.getSimpleName(), method, beanName,
                  beanType.getName());
            }
            else if (!validMethod)
            {
              LOG.error(
                  "Could not subscripe @{}-annotated method '{}' on bean {} [{}]. Annotation can"
                      + " only be used on public non-static methods with return type void and a"
                      + " single parameter.",
                  EventBusListenerMethod.class.getSimpleName(), method, beanName,
                  beanType.getName());
            }

            return validBean && validMethod;
          }).toArray(Method[]::new);

      if (methods.length > 0)
      {
        hasSubscribers = true;
        beanMethodCache.put(beanName, methods);
      }

      return hasSubscribers ? beanScope : null;
    });
  }

  @Override
  public Object postProcessAfterInitialization(Object bean, String beanName)
    throws BeansException
  {
    String beanScope = beanScopeCache.get(beanName);
    if (beanScope == null || beanScope.isEmpty())
      return bean;

    /* determine if bean is also a Vaadin Component */
    Component component = (bean instanceof Component) ? (Component) bean : null;

    EventBus uiScopeEventBus = eventBus.uiScope();

    if (EventBusListener.class.isAssignableFrom(bean.getClass()))
    {
      EventBusSubscription subscription = uiScopeEventBus.subscribe((EventBusListener<?>) bean);

      /*
       * Binding the subscription to a component is only really necessary for the prototype scope.
       *
       * Components in the UI scope will always receive events, unless their subscription is bound
       * to themselves (as Component); in that case they will only receive events when they are
       * actually attached. This can be important e.g. when events are sent during the creation of a
       * view (and its parent layouts) before they are actually attached. Therefore don't bind the
       * subscription when Component beans are in the UI scope.
       */
      if (component != null && !beanScope.equals(VaadinUIScope.VAADIN_UI_SCOPE_NAME))
        subscription.bindTo(component);
    }

    Method[] methods = beanMethodCache.get(beanName);
    if (methods != null)
    {
      for (Method method : methods)
      {
        EventBusSubscription subscription = uiScopeEventBus.subscribe(bean, method);

        /*
         * See above.
         */
        if (component != null && !beanScope.equals(VaadinUIScope.VAADIN_UI_SCOPE_NAME))
          subscription.bindTo(component);
      }
    }

    return bean;
  }

  @Override
  public void resetBeanDefinition(String beanName)
  {
    beanScopeCache.remove(beanName);
    beanMethodCache.remove(beanName);
  }

}
