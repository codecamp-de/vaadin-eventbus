package de.codecamp.vaadin.eventbus.autoconfiguration;


import com.vaadin.flow.component.UI;
import com.vaadin.flow.server.VaadinSession;
import com.vaadin.flow.spring.scopes.VaadinSessionScope;
import com.vaadin.flow.spring.scopes.VaadinUIScope;
import de.codecamp.vaadin.eventbus.EventBus;
import de.codecamp.vaadin.eventbus.EventBusAccess;
import de.codecamp.vaadin.eventbus.EventBusAccessor;
import de.codecamp.vaadin.eventbus.EventBusScope;
import de.codecamp.vaadin.eventbus.EventScope;
import de.codecamp.vaadin.eventbus.impl.EventBusAccessorImpl;
import de.codecamp.vaadin.eventbus.impl.EventBusImpl;
import de.codecamp.vaadin.eventbus.impl.ManagedEventBus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.AutoConfiguration;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.lang.Nullable;


@AutoConfiguration
@EnableConfigurationProperties(EventBusProperties.class)
public class VaadinEventBusAutoConfiguration
{

  @Autowired
  private EventBusProperties properties;


  @Bean
  static EventBusListenerMethodBeanPostProcessor vaadinEventBusListenerMethodBeanPostProcessor(
      @Lazy EventBusAccessor eventBusAccessor)
  {
    return new EventBusListenerMethodBeanPostProcessor(eventBusAccessor);
  }


  @Bean
  EventBusAccessor vaadinEventBusAccessor(
      @EventBusScope(EventScope.APPLICATION) @Nullable EventBus vaadinApplicationEventBus,
      @EventBusScope(
          value = EventScope.SESSION,
          proxy = true) EventBus vaadinProxiedSessionEventBus,
      @EventBusScope(value = EventScope.UI, proxy = true) EventBus vaadinProxiedUiEventBus)
  {
    EventBusAccessorImpl bean = new EventBusAccessorImpl(vaadinApplicationEventBus,
        vaadinProxiedSessionEventBus, vaadinProxiedUiEventBus);
    EventBusAccess.setStrategy(() -> bean);
    return bean;
  }


  @Bean(destroyMethod = "destroy")
  @Nullable
  @EventBusScope(EventScope.APPLICATION)
  ManagedEventBus vaadinApplicationEventBus()
  {
    if (!properties.isApplicationScopeEnabled())
      return null;

    String name = EventScope.APPLICATION.name();
    return new EventBusImpl(name, EventScope.APPLICATION);
  }


  @Bean(destroyMethod = "destroy")
  @Scope(scopeName = VaadinSessionScope.VAADIN_SESSION_SCOPE_NAME, proxyMode = ScopedProxyMode.NO)
  @EventBusScope(EventScope.SESSION)
  ManagedEventBus vaadinSessionEventBus(
      @EventBusScope(EventScope.APPLICATION) ManagedEventBus vaadinApplicationEventBus)
  {
    String name = EventScope.SESSION.name() + ":" + VaadinSession.getCurrent().getSession().getId();
    return new EventBusImpl(name, EventScope.SESSION, vaadinApplicationEventBus);
  }

  @Bean
  @Scope(
      scopeName = VaadinSessionScope.VAADIN_SESSION_SCOPE_NAME,
      proxyMode = ScopedProxyMode.INTERFACES)
  @EventBusScope(value = EventScope.SESSION, proxy = true)
  ManagedEventBus vaadinProxiedSessionEventBus(
      @EventBusScope(EventScope.SESSION) ManagedEventBus vaadinSessionEventBus)
  {
    return vaadinSessionEventBus;
  }


  @Bean(destroyMethod = "destroy")
  @Scope(scopeName = VaadinUIScope.VAADIN_UI_SCOPE_NAME, proxyMode = ScopedProxyMode.NO)
  @EventBusScope(EventScope.UI)
  ManagedEventBus vaadinUiEventBus(
      @EventBusScope(EventScope.SESSION) ManagedEventBus vaadinSessionEventBus)
  {
    String name = EventScope.UI.name() + ":" + VaadinSession.getCurrent().getSession().getId() + ":"
        + UI.getCurrent().getUIId();
    return new EventBusImpl(name, EventScope.UI, vaadinSessionEventBus);
  }

  @Bean
  @Scope(scopeName = VaadinUIScope.VAADIN_UI_SCOPE_NAME, proxyMode = ScopedProxyMode.INTERFACES)
  @EventBusScope(value = EventScope.UI, proxy = true)
  ManagedEventBus vaadinProxiedUiEventBus(
      @EventBusScope(EventScope.UI) ManagedEventBus vaadinUiEventBus)
  {
    return vaadinUiEventBus;
  }

}
