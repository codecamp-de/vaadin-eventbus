package de.codecamp.vaadin.eventbus.autoconfiguration;


import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.validation.annotation.Validated;


@Validated
@ConfigurationProperties(prefix = EventBusProperties.PREFIX)
public class EventBusProperties
{

  public static final String PREFIX = "codecamp.vaadin.eventbus";


  /**
   * Whether the application scope should be enabled.
   */
  private boolean applicationScopeEnabled = false;


  public boolean isApplicationScopeEnabled()
  {
    return applicationScopeEnabled;
  }

  public void setApplicationScopeEnabled(boolean applicationScopeEnabled)
  {
    this.applicationScopeEnabled = applicationScopeEnabled;
  }

}
