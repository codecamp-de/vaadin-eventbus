package de.codecamp.vaadin.eventbus;


import com.vaadin.flow.server.VaadinService;


/**
 * {@link EventBusAccess} is the static access point to an {@link EventBusAccessor} which in turn
 * provides access to the available event scopes in the form of their corresponding {@link EventBus}
 * objects.
 */
public final class EventBusAccess
{

  private static EventBusAccessStrategy strategy = new VaadinContextStrategy();


  private EventBusAccess()
  {
    // utility class
  }


  /**
   * Returns the {@link EventBusAccessor} based on the {@link #setStrategy(EventBusAccessStrategy)
   * configured strategy}.
   * <p>
   * The {@link EventBusAccessor} is not serializable. As such, it should not be stored in any
   * fields of UI components as long as the Vaadin sessions should remain serializable.
   *
   * @return the {@link EventBusAccessor}
   */
  public static EventBusAccessor get()
  {
    return strategy.getEventBusAccessor();
  }


  /**
   * Sets the strategy that is responsible for acquiring an {@link EventBusAccessor} instance based
   * on the context.
   *
   * @param strategy
   *          the new strategy; {@code null} to restore the default strategy
   */
  public static synchronized void setStrategy(EventBusAccessStrategy strategy)
  {
    if (strategy == null)
      EventBusAccess.strategy = new VaadinContextStrategy();
    else
      EventBusAccess.strategy = strategy;
  }


  /**
   * A strategy is responsible for acquiring an {@link EventBusAccessor} instance based on the
   * context.
   */
  public interface EventBusAccessStrategy
  {

    /**
     * Acquires and returns an {@link EventBusAccessor} instance based on the current context.
     *
     * @return an {@link EventBusAccessor} instance based on the current context
     */
    EventBusAccessor getEventBusAccessor();

  }


  /**
   * This default strategy accesses the {@link EventBusAccessor} via the current Vaadin session or
   * UI. So it doesn't work without at least one of them.
   */
  private static class VaadinContextStrategy
    implements
      EventBusAccessStrategy
  {

    @Override
    public EventBusAccessor getEventBusAccessor()
    {
      VaadinService vaadinService = VaadinService.getCurrent();
      if (vaadinService == null)
        throw new IllegalStateException(
            "The VaadinService is not available for the current thread.");

      return vaadinService.getInstantiator().getOrCreate(EventBusAccessor.class);
    }

  }

}
