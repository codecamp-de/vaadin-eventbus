package de.codecamp.vaadin.eventbus;


import com.vaadin.flow.component.Component;
import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import org.springframework.beans.factory.annotation.Autowired;


/**
 * Used on methods to automatically register them as event bus listeners in the UI scope. Generally,
 * this annotation is considered wherever {@link Autowired} is considered.
 * <p>
 * The method must be a public instance method with the return type {@code void}. There must be
 * exactly one method parameter, whose type will determine the type of the events received. The
 * annotated object must either live in the UI scope or be a {@link Component}. If it is a
 * {@link Component} and <em>doesn't</em> live in the UI scope (like Spring's prototype scope), the
 * subscription will be {@link EventBusSubscription#bindTo(Component) bound} to the component's
 * lifecycle.
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface EventBusListenerMethod
{
  // no attributes
}
