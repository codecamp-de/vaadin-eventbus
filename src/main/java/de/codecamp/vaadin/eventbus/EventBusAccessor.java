package de.codecamp.vaadin.eventbus;


/**
 * The {@link EventBusAccessor} combines access to the {@link EventBus} of each available scope.
 * <p>
 * The {@link EventBusAccessor} is not serializable! As such, always use
 * {@link EventBusAccess#get()} to access it instead of direct dependency injection. Otherwise your
 * Vaadin session is no longer serializable.
 */
public interface EventBusAccessor
{

  /**
   * Returns the event bus for the application scope. Thie application scope must be explicitly
   * enabled for this.
   *
   * @return the event bus for the application scope
   * @throws IllegalStateException
   *           if the application scope has not been enabled
   */
  EventBus applicationScope();

  /**
   * Returns the event bus for the session scope.
   *
   * @return the event bus for the session scope
   */
  EventBus sessionScope();

  /**
   * Returns the event bus for the UI scope.
   *
   * @return the event bus for the UI scope
   */
  EventBus uiScope();

}
