package de.codecamp.vaadin.eventbus;


/**
 * The supported scopes for events.
 */
public enum EventScope
{

  /**
   * Events published in the application scope will be visible in the application scope, any session
   * scope and any UI scope. Conversely, listening to the application scope means that only events
   * from the application scope will be received; no events from any session or UI scopes.
   */
  APPLICATION,

  /**
   * Events published in the session scope will be visible in the same session scope and any child
   * UI scopes. Conversely, listening to the session scope means that only events from the
   * application scope and the same session scope will be received; no events from any UI scopes.
   */
  SESSION,

  /**
   * Events published in the UI scope will only be visible in the same UI scope; there are no child
   * scopes. Conversely, listening to the UI scope means that only events from the application
   * scope, the same session scope and the same UI scope will be received; no events from other
   * session scopes or other UI scopes.
   */
  UI

}
